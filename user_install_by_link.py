#!/usr/bin/python

import pathlib
import sys


mNd = pathlib.Path('./math3d')
local = (pathlib.Path.home() /
         pathlib.Path(f'.local/lib/python{sys.version_info.major}.' +
                      f'{sys.version_info.minor}/site-packages'))
local.mkdir(parents=True, exist_ok=True)
mNdl = local / 'math3d'
if mNdl.exists() and mNdl.resolve() != mNd.absolute():
    print(f'mNd link currently points to {mNdl.resolve()}. ' +
          f'Re-linking to {mNd.absolute()}.')
    mNdl.unlink()
if not mNdl.exists():
    print(f'Linking {mNdl} to {mNd.absolute()}.')
    mNdl.symlink_to(mNd.absolute())
else:
    print('Link was already correct.')
